#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

uint8_t is_little_endian(void)
{
    union tricky_issues
    {
        uint8_t a;
        uint16_t b;
    }test_u;
    test_u.b = 1;
    return test_u.a;
}

//Never try to return address of local variable
/*
int *inc(int val)
{
    int a = val;
    a++;
    return &a;
} */

void func1(void)
{
    printf("This is func1\n");
}
void func2(void)
{
    printf("This is func2 \n");
}

bool getmemeory(uint8_t **p)
{
    *p = (int *)malloc(10);
    if(*p != NULL) 
        return true;
    else 
        return false;
}

void swap(int **pa, int **pb)
{
    int *p_temp;
    p_temp = *pa;
    *pa = *pb;
    *pb = p_temp;
}

int main(int argc, char *argv[])
{
    //  test1: this test is used for test C operator priority
    //  please refer to www.slyar.com/bolg/c-opeator-priority.html for details  
    /* Test 1.1
    int a,b,c,d;
    a = 10;
    b = a++;
    c = ++a;
    d = 10 * a++;

    printf("a:%d, b:%d, C:%d, d:%d\n", a,b,c,d);
    // 13, 10, 12, 120
    return 0;*/

    /* Test 1.2 another example 
    char *ptr = "Linux";

    printf("\n[%c]\n", *ptr++);
    printf("\n[%c]\n", *ptr);
    return 0; */

     //Test 1.3 
     
    /* int a = 10;
    int b = 11;
    int *aa[10] = {NULL};  //aa is an array and each element of array is a pointer points to an integer
    aa[0] = &a;
    aa[1] = &b;
    printf(" %d, %d\n", *aa[0], *aa[1]);

    int arrary_t[2][10] = {{1,2,3,4,5,6,7,8,9,0},{11,12,13,14,15,1,17,18,19,20}};
    int (*ab)[10];   //ab is a pointer and points to 
    ab = &arrary_t[1];
    printf("%d,%d\n",(*ab)[0], (*ab)[1]);

    void (*fn[2]) (void); //fn is an array and each elment of array is a pointer points to a function
    fn[1] = func1;
    fn[2] = func2;
    fn[1]();
    fn[2]();
    
    typedef void(*pf)(void);
    pf PF = func1;
    PF();*/
    

    //test 2
    /*
    char *src = "Hello world";
    int len= strlen(src);

    printf("len is %d\n", len);
    char *dest = (char *)malloc(len+1);

    char *d = dest;
    char *s = &src[len-1];

    while(len-- != 0)
    {
        *d++ = *s--;
    }
    *d = '\0';
    printf("%s\n", dest);

    free(dest);
    dest = NULL;

    return 0;
    */

    //test3  a test to test memory alignment
    /* 
   typedef struct{
        uint8_t a;
        uint16_t b;
        uint32_t c;
   }__attribute__((aligned(1),packed)) data_t;

   union{
    uint8_t buf[7];
    data_t s_data;
   }my_union;

    my_union.s_data.a = 1;
    my_union.s_data.b = 13;
    my_union.s_data.c = 0xFFFFFAFE;
   
    printf("size of struct is %d\n", (int)(sizeof(my_union)));
    
    for(uint8_t i = 0; i < 7; i++)
    {   
        printf("%02x\n", my_union.buf[i]);
    }

    return 0;*/

    //test 4
    /* if(is_little_endian())
        printf("mint x86 is little endian\n");
    else
        printf("mint x86 is not little endian\n");*/

    //test 5  return address of local virable is dangerous
    /* int test_data = 10;
    int *value = inc(test_data);
    printf("Incremented value is equal to %d\n", *value);*/

    // Test 6  pointer to pointer
    /* uint8_t a = 1;
    uint8_t *p1 = &a;
    uint8_t **p2 = &p1;

    printf("0x%04x, %d \n", (uint32_t)(&a), a);
    printf("0x%04x, %d \n", (uint32_t)p1,*p1);
    printf("0x%04x, %d \n", (uint32_t)p2,**p2);*/

    /* uint8_t *p_dest = NULL;
    getmemeory(&p_dest);
    uint8_t *p_cpy = p_dest;
    for(uint8_t i = 0; i< 10; i++)
    {
        *p_cpy++ = i;
    }
    printf("data 0 is %d\n", p_dest[0]);
    printf("data n is %d\n", p_dest[9]);
    free(p_dest);*/

    /* 
    int a = 3, b = 4;
    int *p1= &a; int *p2 = &b;
    //printf("a:%d, b:%d\n", *p1, *p2);
    printf("a:%d, b:%d\n", a, b);
    swap(&p1, &p2);
    //printf("a:%d, b:%d\n", *p1, *p2);
    printf("a:%d, b:%d\n", a, b);
    */
    return 0; 
}
